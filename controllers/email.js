var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/salonPostHelper");
var getHelper = require("../helpers/salonGetHelper");
var putHelper = require("../helpers/salonPutHelper");

const knex = require("knex");

router
  .route("/")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .post(async function(req, res) {
    let response = await postHelper.salonEmailInsert(req);
    res.json(response);
  });

router
  .route("/:salon_email_id")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .put(async function(req, res) {
    let response = await putHelper.salonEmailUpdate(req);
    res.json(response);
  });

router
  .route("/getByID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.salonGetEmailData(req, "id");
    res.json(response);
  });
router
  .route("/getBysalonID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.salonGetEmailData(req, "salon_id");
    res.json(response);
  });

module.exports = router;
