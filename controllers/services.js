var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/salonPostHelper");
var getHelper = require("../helpers/salonGetHelper");
var putHelper = require("../helpers/salonPutHelper");

const knex = require("knex");

router
  .route("/")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .post(async function(req, res) {
    let response = await postHelper.servicesInsert(req);
    res.json(response);
  });


router
  .route("/getByID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.salonGetServicesData(req, "id");
    res.json(response);
  });

  router
  .route("/getAllData/")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.salonServicesAllData(req);
    res.json(response);
  });
  

  router
  .route("/putByID/:id")
  // fetch all addresses
  .put(async function(req, res) {
    let response = await putHelper.servicesUpdate(req, "id");
    res.json(response);
  });

module.exports = router;
