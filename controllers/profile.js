var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/salonPostHelper");
var getHelper = require("../helpers/salonGetHelper");
var putHelper = require("../helpers/salonPutHelper");

const knex = require("knex");

router
  .route("/")
  // fetch all addresses
  .post(async function(req, res) {
    let response = await postHelper.salonProfileInsert(req);
    res.json(response);
  });

router
  .route("/:salon_profile_id")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .put(async function(req, res) {
    let response = await putHelper.salonProfileUpdate(req);
    res.json(response);
  });

  router.route("/salonID/:salon_id").put(async function(req, res) {
    let response = await putHelper.salonProfilePut(req);
    res.json(response);
  });

router
  .route("/getByID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.salonGetProfileData(req, "id");
    res.json(response);
  });

  router.route("/razorpaycontact/").post(async function(req, res) {
    let response = await postHelper.razorpayPostContact(req);
    res.json(response);
    console.log(response);
  });

  router.route("/razorpayfund/bank").post(async function(req, res) {
    let response = await postHelper.razorpayPostFundAccBank(req);
    res.json(response);
    console.log(response);
  });

  router.route("/razorpayfund/vpa").post(async function(req, res) {
    let response = await postHelper.razorpayPostFundAccVpa(req);
    res.json(response);
    console.log(response);
  });

  router.route("/razorpaycontact/:employee_id").put(async function(req, res) {
    let response = await putHelper.razorpayPutContact(req);
    res.json(response);
  });

router
  .route("/getBysalonID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.salonGetProfileData(req, "salon_id");
    res.json(response);
  });

module.exports = router;
