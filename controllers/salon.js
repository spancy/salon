var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/salonPostHelper");
var getHelper = require("../helpers/salonGetHelper");
var putHelper = require("../helpers/salonPutHelper");

const knex = require("knex");

router
  .route("/")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .post(async function(req, res) {
    let response = await postHelper.salonInsert(req);
    res.json(response);
  });

router.route("/sendEmail").post(async function(req, res) {
  let response = await postHelper.sendInvoiceSes(req);
  res.json(response);
});

router
  .route("/:salon_id")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .put(async function(req, res) {
    let response = await putHelper.salonUpdate(req);
    res.json(response);
  });

router
  .route("/all")
  // fetch all addresses
  .get(async function(req, res) {
    //let response = await getHelper.salonGetAllData(req);
    let response = await getHelper.salonGetAllDataByService(req);
    res.json(response);
  });

router
  .route("/all/byLocation")
  // fetch all addresses
  .get(async function(req, res) {
    //let response = await getHelper.salonGetAllData(req);
    let response = await getHelper.salonGetAllDataByLocation(req);
    res.json(response);
  });

router
  .route("/getByEmailID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.salonGetData(req, "registered_email");
    res.json(response);
  });

router
  .route("/getByPhoneNumber/:phone_number")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.salonGetData(req, "phone_number");
    res.json(response);
  });

router
  .route("/getByID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.salonGetData(req, "id");
    res.json(response);
  });
module.exports = router;
