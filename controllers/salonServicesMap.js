var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/salonPostHelper");
var getHelper = require("../helpers/salonGetHelper");
var putHelper = require("../helpers/salonPutHelper");

const knex = require("knex");

router
  .route("/")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .post(async function(req, res) {
    let response = await postHelper.salonServicesMapInsert(req);
    res.json(response);
  });


router
  .route("/getByID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.salonServicesMapData(req, "id");
    res.json(response);
  });

  router
  .route("/getBySalonID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.salonServicesMapData(req, "salon_id");
    res.json(response);
  });

  router
  .route("/getByServiceID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.salonServicesMapData(req, "service_id");
    res.json(response);
  });

router
  .route("/putBysalonIDServiceID/:salon_id/:service_id")
  // fetch all addresses
  .put(async function(req, res) {
    let response = await putHelper.salonServicesMapUpdate(req, "salon_id","service_id");
    res.json(response);
  });

  router
  .route("/deleteBysalonIDServiceID/:salon_id/:service_id")
  // fetch all addresses
  .put(async function(req, res) {
    let response = await putHelper.salonServicesMapDelete(req, "salon_id","service_id");
    res.json(response);
  });

  //router
  //.route("/putByServiceID/:id")
  // fetch all addresses
  //.put(async function(req, res) {
   // let response = await putHelper.salonServicesMapUpdate(req, "service_id");
   // res.json(response);
  //});

module.exports = router;
