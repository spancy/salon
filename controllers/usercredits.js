var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/salonPostHelper");
var getHelper = require("../helpers/salonGetHelper");
var putHelper = require("../helpers/salonPutHelper");

const knex = require("knex");

router.route("/").post(async function(req, res) {
  let response = await postHelper.userCreditsInsert(req);
  res.json(response);
});

router.route("/:id").put(async function(req, res) {
  let response = await putHelper.userCreditsUpdate(req);
  res.json(response);
});

router.route("/salon/salon_id").put(async function(req, res) {
  let response = await putHelper.userCreditsUpdateBysalonID(req);
  res.json(response);
});

/*router.route("/").get(async function(req, res) {
  let response = await getHelper.employeeAddressAllData(req);
  res.json(response);
});*/

router.route("/getByID/:id").get(async function(req, res) {
  let response = await getHelper.salonGetUserCreditsData(req, "id");
  res.json(response);
});

router.route("/getBysalonID/:id").get(async function(req, res) {
  let response = await getHelper.salonGetUserCreditsData(req, "salon_id");
  res.json(response);
});

module.exports = router;
