//GET - {status: status, message: message, usernames : [], registeredEmails : []}
// status can be 200 or 500
// message can be  "Success" or "Internal Server error"
//POST - {status : status, message : message}
// status - 403 if any of email, username or password field is empty
// status - 500 if any other input field is missing or any other server error occurs
// status - 409 if user or email is already registered
// status - 200 if everything succeeds
// message corresponds to the error, or "User details saved successfully and activation mail sent" if everything succeeds

var express = require('express');
var UserAuthentication = require('../db/models/userAuthentication');
var configFile = require('./config')

var router = express.Router();

var crypto = require('crypto')
var promise = require('bluebird')
var bcrypt = require('bcrypt-nodejs')
var mailer = require('nodemailer-promise');
//var speakeasy = require('speakeasy')

var usernameList = []
var registeredEmailList = []

var userID=''

router.get('/', function(req, res, next){

	UserAuthentication.fetchAll({
		columns: ['username', 'registeredEmail']
	}).then(function(result){

		for(var i=0; i<result.models.length;i++){
			usernameList.push(result.models[i].attributes.username)
			registeredEmailList.push(result.models[i].attributes.registeredEmail)
		}

		res.json({status:200,usernames:usernameList,registeredEmails:registeredEmailList,message:"Success"})
	}).catch(function(error){
		res.json({status:500, usernames:[], registeredEmails:[], message:"Internal Server error"})
	})
})


router.post('/', function(req,res, next){

		var userVerificationHash = ''
		var status = ''
		var message = ''

		var mailErrorFlag = false
		var dataInsertionErrorFlag = false
		var smsErrorFlag = false

		var inputProblemFlag = false

		var data = {

			username : '',
			password : '',
			phoneNumber : '',
			registeredEmail : '',
			userVerificationHash : '',
			isOTPVerified : '',
			emailVerified : '',
			user_role_id : '',
			otpSecret : ''
		};

		var server = req.get('host')

		if(!req.body.username){
			return res.json({status:403,message:"Username missing"})
		}
		else{
			data.username = req.body.username
		}

		if(!req.body.password){
			return res.json({status:403,message:"Password missing"})
		}
		else{
			data.password = req.body.password
		}

		if(!req.body.registeredEmail){
			return res.json({status:403,message:"Mail ID missing"})
		}
		else{
			data.registeredEmail = req.body.registeredEmail
		}

		if(!req.body.phoneNumber){
			return res.json({status:500, message:"Phone Number missing"})
		}
		else{
			data.phoneNumber = req.body.phoneNumber
		}



		UserAuthentication.fetchAll({
			columns : ['username','registeredEmail','phoneNumber']
		}).then(function(result) {


				for(var i=0; i<result.models.length;i++){
					if (data.username == result.models[i].attributes.username){
						return res.json({status:409,message:"Username already registered"})
					}
					if (data.registeredEmail == result.models[i].attributes.registeredEmail){
						return res.json({status:409,message:"Email already registered"})
					}
					if (data.phoneNumber == result.models[i].attributes.phoneNumber){
						return res.json({status:409,message:"Phone number registered"})
					}
				}


				data.isOTPVerified = false
				data.emailVerified = false
				data.user_role_id = req.body.user_role_id

                                var salt = bcrypt.genSaltSync(12);
				var hashGenerationPromise = promise.promisify(crypto.randomBytes)
				hashGenerationPromise(configFile.bytesForHash).then(function(buffer){

					return buffer.toString('hex')


				}).then(function(result) {

					data.userVerificationHash = result
					userVerificationHash = result
					return bcrypt.hashSync(req.body.password, salt)

				}).then(function(result){

					//var secret = speakeasy.generateSecret({length:20});
					dataInsertionErrorFlag = true

					data.password = result
					//data.otpSecret = secret.base32
					var userAuthentication = new UserAuthentication()
					return userAuthentication.save(data)

				}) .then(function(result){
userID=result.get('id')
var userDetails =result.toJSON()
						console.log(userDetails)
					dataInsertionErrorFlag = false
					mailErrorFlag = true

					var sendEmail = mailer.config({
					    email: configFile.senderMail,
					    password: configFile.senderMailPassword,
					    server: configFile.senderServer
					});

					var options = {
					    subject: 'UrbanHealth - Activation link',
					    senderName: configFile.senderName,
					    receiver: data.registeredEmail,
					    html: '<!DOCTYPE html><html>\
					    <body>\
					    <p>Thank you for registering with UrbanHealth. Please click on the link below to activate your account</p>\
					    </br>\
						  <a href=\"http://www.urbanhealth.co.in/#/registrationsuccess?username='+userDetails.username+'&key=' + userVerificationHash + '\">Confirmation</a>\
					    </body>\
					    </html>'
					};



					return sendEmail(options)

				})

				.then(function(result){

					mailErrorFlag = false
			        	smsErrorFlag = true

/** 2factor code **/


var http = require("http");



var options = {

  "method": "GET",

  "hostname": "2factor.in",

  "port": null,

  "path": "/API/V1/"+configFile.otpkey+"/SMS/"+data.phoneNumber+"/AUTOGEN",

  "headers": {}

};



var req = http.request(options, function (res) {

  var chunks = [];



  res.on("data", function (chunk) {

    chunks.push(chunk);

  });



  res.on("end", function () {

    var body = Buffer.concat(chunks);

    var obj = JSON.parse(body);

    console.log(obj.Details);

		UserAuthentication
		.where({id:userID})
		.save({otpSecret: obj.Details,isOTPVerified: false},{patch:true})
		.then(function(result){
			smsErrorFlag=false
		})
		.catch(function(error){
			smsErrorFlag=true
		})
	return 	obj.Details

  });

});



req.write("{}");

req.end();

/**End 2factor code**/

			/*		var token = speakeasy.totp({
					  secret: data.otpSecret,
					  encoding: 'base32',
					})

					var twilio = require('twilio');
					var client = new twilio(configFile.smsSID, configFile.smsToken);

					return client.messages.create({
					    body: "Your OTP is " + token + " .",
					    to: configFile.phoneIndiaCode + data.phoneNumber,  // Text this number
					    from: configFile.twilioPhoneNumber // From a valid Twilio number*/
					})

				.then(function(result){


					status = 200
					message = "User details saved successfully, sms and activation mail sent"




				}).catch(TypeError,ReferenceError,RangeError,function(error){

					status = 500
					message = "Coding Error"+error

				}).catch(function(error){

					status = 500

					if(dataInsertionErrorFlag){
						message = "Data insertion error"
					}

					if(mailErrorFlag){
                                               console.log(error);
						message = "Mail error"
					}


					if(smsErrorFlag){
						console.log(error);
						message = "SMS error"
					}


				}).finally(function(){

					res.json({id:userID,status:status,message:message})
				})


		})

});

module.exports = router;
