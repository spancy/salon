var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/salonPostHelper");
var getHelper = require("../helpers/salonGetHelper");
var putHelper = require("../helpers/salonPutHelper");

const knex = require("knex");

router
  .route("/")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .post(async function(req, res) {
    let response = await postHelper.professionalInsert(req);
    res.json(response);
  });

router
  .route("/getByID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.getProfessionalData(req, "id");
    res.json(response);
  });

router
  .route("/getBySalonID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.getProfessionalData(req, "salon_id");
    res.json(response);
  });

router
  .route("/getAvailableProfessionals")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.getAvailableProfessionals(req);
    res.json(response);
  });

router
  .route("/putBysalonID/:id")
  // fetch all addresses
  .put(async function(req, res) {
    let response = await putHelper.professionalUpdate(req, "salon_id");
    res.json(response);
  });

  router
  .route("/deleteProfessional/:salon_id/:professional_id")
  // fetch all addresses
  .put(async function(req, res) {
    let response = await putHelper.professionalDelete(req, "salon_id","professional_id");
    res.json(response);
  });

router
  .route("/putByprofessionalID/:id")
  // fetch all addresses
  .put(async function(req, res) {
    let response = await putHelper.professionalUpdate(req, "id");
    res.json(response);
  });

module.exports = router;
