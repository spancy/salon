var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/salonPostHelper");
var getHelper = require("../helpers/salonGetHelper");
var putHelper = require("../helpers/salonPutHelper");

const knex = require("knex");

router
  .route("/")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .post(async function(req, res) {
    let response = await postHelper.salonPhoneInsert(req);
    res.json(response);
  });

router
  .route("/:salon_phone_id")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .put(async function(req, res) {
    let response = await putHelper.salonPhoneUpdate(req);
    res.json(response);
  });

router
  .route("/getByID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.salonGetPhoneData(req, "id");
    res.json(response);
  });
router
  .route("/getBysalonID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.salonGetPhoneData(req, "salon_id");
    res.json(response);
  });

module.exports = router;
