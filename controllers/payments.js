var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/salonPostHelper");
var getHelper = require("../helpers/salonGetHelper");
var putHelper = require("../helpers/salonPutHelper");

const knex = require("knex");

router
  .route("/")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .post(async function(req, res) {
    let response = await postHelper.salonPaymentsInsert(req);
    res.json(response);
  });

router
  .route("/:salon_payments_id")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .put(async function(req, res) {
    let response = await putHelper.salonPaymentsUpdate(req);
    res.json(response);
  });

router
  .route("/getByID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.salonGetPaymentsData(req, "id");
    res.json(response);
  });
router
  .route("/getBysalonID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.salonGetPaymentsData(req, "salon_id");
    res.json(response);
  });

module.exports = router;
