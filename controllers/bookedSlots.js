var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/salonPostHelper");
var getHelper = require("../helpers/salonGetHelper");
var putHelper = require("../helpers/salonPutHelper");

const knex = require("knex");

router
  .route("/")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .post(async function(req, res) {
    let response = await postHelper.bookedslotsInsert(req);
    res.json(response);
  });


router
  .route("/getByID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.bookedSlotsData("id", req);
   res.json(response);
 });

  router
 .route("/all")
 // fetch booked Slots by parameters ( Date and Salon ID)
 .get(async function(req, res) {
   let response = await getHelper.bookedSlots(req);
    res.json(response);
  });

  router
  .route("/getBySalonID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.bookedSlotsData("salon_id", req);
    res.json(response);
  });

router
  .route("/putBysalonID/:id")
  // fetch all addresses
  .put(async function(req, res) {
    let response = await putHelper.bookedSlotsUpdate(req, "salon_id");
    res.json(response);
  });

  router
  .route("/putByslotID/:id")
  // fetch all addresses
  .put(async function(req, res) {
    let response = await putHelper.bookedSlotsUpdate(req, "id");
    res.json(response);
  });

module.exports = router;
