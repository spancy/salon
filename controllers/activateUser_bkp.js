//GET - {status : status, message : message, username : username }
// status - 200 if everything is fine
// status - 500 if any server error occurs
// message - "Success" or "Failure" if everything succeeds


var express = require('express');
var UserAuthentication = require('../db/models/userAuthentication');

var router = express.Router();
var http = require('http')

router.get('/', function(req,res, next){

    var id = ''
    var username = ''
    var phoneNumber = ''
    var registeredEmail = ''

	var userVerificationHashValue = req.query.key

	UserAuthentication
	.where({userVerificationHash:userVerificationHashValue})
    .save({emailVerified: true},{patch:true})
    .then(function(result) {

    UserAuthentication.
    where({userVerificationHash:userVerificationHashValue}).
    fetch()
    .then(function(result){

        id = result.attributes.id
        username = result.attributes.username
        phoneNumber = result.attributes.phoneNumber
        registeredEmail = result.attributes.registeredEmail

        // API CALL to /userprofile to update "isEmailVerified"

        var userData = {
            'id' : id,
            'userName' : username
        }

        console.log(userData)

        var phoneNumberData = {
            'user_id' : id,
            'number' : phoneNumber,
            'verified' : true
        }

        var emailData = {
            'user_id' : id,
            'emailAddress' : registeredEmail,
            'verified' : true
        }

        var userDataInString = JSON.stringify(userData)
        var phoneNumberDataInString = JSON.stringify(phoneNumberData)
        var emailDataInString = JSON.stringify(emailData)

          var serverArray = req.get('host').split(':')

          var host = serverArray[0]

          var useroptions = {
            host: host,
            port: 8888,
            path: '/users',
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Content-Length': Buffer.byteLength(userDataInString)
          }
          };

          var userrequest = http.request(useroptions, (userresult) => {

            userresult.setEncoding('utf8');
            userresult.on('data', function (user) {

                var userResponse = JSON.parse(user)
                console.log(userResponse)
                if(userResponse['error']) {

                    res.json({error: true, data: userResponse['data']['message']});

                }
                else{

                    var emailoptions = {
                    host: host,
                    port: 8888,
                    path: '/users/'+id+'/email',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Content-Length': Buffer.byteLength(emailDataInString)
                    }
                  };

                      var emailrequest = http.request(emailoptions, (emailresult) => {

                        emailresult.setEncoding('utf8');
                        emailresult.on('data', function (email) {

                                var emailResponse = JSON.parse(email)
                                if(emailResponse['error']){

                                    res.json({error: true, data: emailResponse['data']['message']});

                                }
                                else{

                    //      })
                    //  })
                                   var phoneoptions = {
                                        host: host,
                                        port: 8888,
                                        path: '/users/'+id+'/phone',
                                        method: 'POST',
                                        headers: {
                                            'Content-Type': 'application/json',
                                            'Content-Length': Buffer.byteLength(phoneNumberDataInString)
                                        }
                                      };

                                     var phonerequest = http.request(phoneoptions, (phoneresult) => {

                                        phoneresult.setEncoding('utf8');
                                        phoneresult.on('data', function (phone) {

                                            var phoneResponse = JSON.parse(phone)
                                            if(phoneResponse['error']){
                                                res.json({error: true, data: phoneResponse['data']['message']});
                                            }
                                            else{
                                                res.json({error: false, data: 'User activated and details saved'});
                                            }

                                          })

                                      })

                                    phonerequest.on('error', function(err) {
                                       res.json({error: true, data: {message: err.message}});
                                     });

                                     phonerequest.write(phoneNumberDataInString)

                                     phonerequest.end()

                                    }

                                  })

                  })

                  emailrequest.on('error', function(err) {
                     res.json({error: true, data: {message: err.message}});
                  });

                  emailrequest.write(emailDataInString)

                  emailrequest.end()
                }

          })
        })

          userrequest.on('error', function(err) {
             res.json({error: true, data: {message: err.message}});
          });

          userrequest.write(userDataInString);

          userrequest.end()

        }).catch(function(error){
            console.log(error)
            res.json({status:500,message:"Failure"})
        });


      // res.json({status:200,message:"Success"})
    }).catch(function(error){
        console.log(error)
    	res.json({status:500,message:"Failure"})
    });
});
// API CALL to /userprofile to send phoneNumber and registeredEmail (decide before or after)
module.exports = router;
