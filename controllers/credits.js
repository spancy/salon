var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/salonPostHelper");
var getHelper = require("../helpers/salonGetHelper");
var putHelper = require("../helpers/salonPutHelper");

const knex = require("knex");

router.route("/").post(async function(req, res) {
  let response = await postHelper.creditsInsert(req);
  res.json(response);
});

router.route("/:id").put(async function(req, res) {
  let response = await putHelper.creditsUpdate(req);
  res.json(response);
});

router.route("/").get(async function(req, res) {
  let response = await getHelper.saloncreditsAllData(req);
  res.json(response);
});

router.route("/getByID/:id").get(async function(req, res) {
  let response = await getHelper.salonGetCreditsData(req, "id");
  res.json(response);
});

module.exports = router;
