var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/salonPostHelper");
var getHelper = require("../helpers/salonGetHelper");
var putHelper = require("../helpers/salonPutHelper");

const knex = require("knex");

router.route("/").post(async function(req, res) {
  let response = await postHelper.userReferralsInsert(req);
  res.json(response);
});

router.route("/:id").put(async function(req, res) {
  let response = await putHelper.userReferralsUpdate(req);
  res.json(response);
});

router.route("/putByReferredTo/:id").put(async function(req, res) {
  let response = await putHelper.userReferralsUpdateByReferredTo(req);
  res.json(response);
});

router.route("/").get(async function(req, res) {
  let response = await getHelper.userReferralsAllData(req);
  res.json(response);
});

router.route("/getByID/:referral_id").get(async function(req, res) {
  let response = await getHelper.salonGetUserReferralsData(req, "referral_id");
  res.json(response);
});

router.route("/getBysalonID/:referred_to").get(async function(req, res) {
  let response = await getHelper.salonGetUserReferralsData(req, "referred_to");
  res.json(response);
});
module.exports = router;
