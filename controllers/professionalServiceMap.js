var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/salonPostHelper");
var getHelper = require("../helpers/salonGetHelper");
var putHelper = require("../helpers/salonPutHelper");

const knex = require("knex");

router
  .route("/")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .post(async function(req, res) {
    let response = await postHelper.professionalServicesMapInsert(req);
    res.json(response);
  });


router
  .route("/getByID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.professionalServicesMapData(req, "id");
    res.json(response);
  });

  router
  .route("/getByProfessionalID/:professional_id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.professionalServicesMapData(req, "professional_id");
    res.json(response);
  });

  router
  .route("/getByServiceID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.professionalServicesMapData(req, "service_id");
    res.json(response);
  });

router
  .route("/getBySalonServiceID/:salon_services_map_id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await putHelper.professionalServicesMapData(req, "salon_services_map_id");
    res.json(response);
  });

  //router
  //.route("/putByServiceID/:id")
  // fetch all addresses
  //.put(async function(req, res) {
   // let response = await putHelper.salonServicesMapUpdate(req, "service_id");
   // res.json(response);
  //});

module.exports = router;
