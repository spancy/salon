var express = require('express');
var UserRoles = require('../db/models/userRoles');

var router = express.Router();

// fetch all roles
router.get('/',function (req, res) {

  UserRoles
  .fetchAll()
  .then(function (collection) {
    res.json({error: false, data: collection.toJSON()});
  })
  .catch(function (err) {
    res.status(500).json({error: true, data: {message: err.message}});
  });
})

module.exports = router;
