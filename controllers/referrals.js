var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/salonPostHelper");
var getHelper = require("../helpers/salonGetHelper");
var putHelper = require("../helpers/salonPutHelper");

const knex = require("knex");

router.route("/").post(async function(req, res) {
  let response = await postHelper.referralsInsert(req);
  res.json(response);
});

router.route("/generate").post(async function(req, res) {
  let response = await postHelper.referralGenerator(req);
  res.json(response);
});

router.route("/:id").put(async function(req, res) {
  let response = await putHelper.salonReferralsUpdate(req);
  res.json(response);
});

router.route("/putBysalonID/:id").put(async function(req, res) {
  let response = await putHelper.salonReferralsUpdateBysalon(req);
  res.json(response);
});

router.route("/").get(async function(req, res) {
  let response = await getHelper.salonreferralsAllData(req);
  res.json(response);
});

router.route("/getByID/:referral_code").get(async function(req, res) {
  let response = await getHelper.salonGetReferralsData(req, "referral_code");
  res.json(response);
});

router.route("/getBysalonID/:salon_id").get(async function(req, res) {
  let response = await getHelper.salonGetReferralsData(req, "salon_id");
  res.json(response);
});

router.route("/getByReferralID/:id").get(async function(req, res) {
  let response = await getHelper.salonGetReferralsData(req, "id");
  res.json(response);
});

module.exports = router;
