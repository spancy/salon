var express = require('express');
var jsonWebToken = require('jsonwebtoken')
var router = express.Router();
var RolePermissions = require('../db/models/rolePermissions');
var UserAuthentication = require('../db/models/userAuthentication');
var EndPoints = require('../db/models/endpoints')
var endPointServiceMap = require('../db/models/endPointServiceMap')
var Services = require('../db/models/services')

router.post('/', function(req,res, next){

	var token = req.body.token;
	var endpoint = req.body.url

	var endpointID = ''
	var endpointService = ''
	var request_type = 'get' // Hardcoded for now, will be changed later 



	if (token) {

	    jsonWebToken.verify(token, 'abc', function(err, decoded) {      
	    	
	      if (err) {
	      	
	        return res.json({ status: 500, message: 'Failed to authenticate token' });    

	      } else {

		        new EndPoints({'endpoint': endpoint})
			  	.fetch()
			  	.then(function(result) {
			  		if (result){
				  		endpointID = result.id
				  		return endpointID
				  	}
				  	else{
				  		return res.json({status:500,message:"Undefined endpoint"})
				  	}

				}).then(function(result){
					
					if (result) {
						if(request_type == 'get'){
							
							return new RolePermissions({'roleID':decoded.user_role_id,'endpointID':result,'get':true}).fetch()
						}
						else if(request_type == 'post'){
							return new RolePermissions({'roleID':decoded.user_role_id,'endpointID':result,'post':true}).fetch()
						}
						else if(request_type == 'put'){
							return new RolePermissions({'roleID':decoded.user_role_id,'endpointID':result,'put':true}).fetch()
						}
						else{
							return new RolePermissions({'roleID':decoded.user_role_id,'endpointID':result,'delete':true}).fetch()
						}
					}
					else{
						return res.json({status:500,message:"Internal Server Error"})
					}
					
					
				}).then(function(result){

					if (!result){

						return res.json({ status: 500, message: 'Unauthorised Access' });  

					}
					else{

						return new endPointServiceMap({'endpointID':endpointID}).fetch()
					}
					

				}).then(function(result){

					if (result){
						endpointServiceID = result.get('serviceID')
						return endpointServiceID
					}
					else{

						return res.json({status:500,message:"Endpoint does not belong to any service"})
					}


				}).then(function(result){

					if (result){
						return new Services({'id':result}).fetch()
					}
					else{
						return res.json({status:500,message:"Internal Server Error"})
					}

				}).then(function(result){

					if (result){
						serviceUrl = result.get('serviceUrl')
						res.redirect('/redirect');
					}
					else{
						return res.json({status:500,message:"Undefined service"})
					}

				}).catch(function(error){
					
					return res.json({status:500,message:"Internal Server Error"})
				});
					

			}

	        
	      
	    });

	  }
});

module.exports = router;