// POST - {status : status, message : message, token : token}
// status - 500 for any server error, 401 for incorrect username,email or password or if user is not yet verified, 200 if everything is fine
// message - For status code 500 - "Internal Server error"
// message - For status code 401 - 'Incorrect username or email', 'User not verified','Incorrect password'
// message - For status code 200 - 'Success'

var express = require('express');
var UserAuthentication = require('../db/models/userAuthentication');
var configFile = require('./config')

var router = express.Router();
var bcrypt = require('bcrypt-nodejs')
var jsonWebToken = require('jsonwebtoken')

var passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;



passport.use(new LocalStrategy(
  function(username, password, done) {

    var emailRegex = new RegExp(/@\S+./);
    var phoneNoRegex = new RegExp(/^\d+$/);

    var emailMatch = username.match(emailRegex)
    var phoneNoMatch = username.match(phoneNoRegex)

    var databaseMatchParameter = {}

    if(emailMatch){
        databaseMatchParameter = {'registeredEmail':username}
    }
    else if(phoneNoMatch){
         databaseMatchParameter = {'phoneNumber':username}
    }
    else{
        databaseMatchParameter = {'username':username}
    }

    new UserAuthentication(databaseMatchParameter)
    .fetch()
    .then(function(result){
    	if(!result){
    		return done(null, false, { message: 'Incorrect username or email' });
    	}

    	if(!result.attributes.emailVerified){
    		return done(null, false, { message: 'User not verified' });
    	}
      else {
      var res = bcrypt.compareSync(password, result.attributes.password)
       if(res){
              return done(null, result);
               }
        else{
              return done(null, false, { message: 'Incorrect password' });
             }
           }
       })
       .catch(function(error){
       console.log(error)
       return done(error)
   })

}

));

router.post('/', function(req,res, next){
	passport.authenticate('local', function(err, user, info) {

    if(err){
    	return res.json({status:500,message:"Internal Server error",data:''})
    }
    if(!user){
        return res.json({status:401,message:info.message,data:''})
    }

    token = jsonWebToken.sign(user.toJSON(), 'abc',{ expiresIn: '2h' });
    return res.json({status:200,message:"Success",data:user.toJSON()})


  })(req, res, next);


})

module.exports = router;
