"use strict";

const Model = require("objection").Model;
const knex = require("knex");

const KnexConfig = require("../knexfile");

Model.knex(knex(KnexConfig.development));
class ProfessionalServiceMap extends Model {
  // Table name is the only required property.
  static get tableName() {
    return "professional_services_map";
  }

  static get relationMappings() {
    return {
      salon_services_map: {
        relation: Model.BelongsToOneRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/Salon",
        join: {
          from: "professional_services_map.salon_services_map_id",
          to: "salon_services_map.id"
        }
      },

      professional: {
        relation: Model.BelongsToOneRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/Professional",
        join: {
          from: "professional_services_map.professional_id",
          to: "professional.id"
        }
      },

      service: {
        relation: Model.BelongsToOneRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/Services",
        join: {
          from: "professional_services_map.service_id",
          to: "services.id"
        }
      }
    };
  }
  

}
module.exports = ProfessionalServiceMap;
