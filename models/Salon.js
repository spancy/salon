"use strict";

const Model = require("objection").Model;
const knex = require("knex");

const KnexConfig = require("../knexfile");

Model.knex(knex(KnexConfig.development));
class Salon extends Model {
  // Table name is the only required property.
  static get tableName() {
    return "salon_authentication";
  }

  static get relationMappings() {
    return {
      profile: {
        relation: Model.BelongsToOneRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/SalonProfile",
        join: {
          from: "salon_profile.salon_id",
          to: "salon_authentication.id"
        }
      },
      address: {
        relation: Model.HasManyRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/SalonAddress",
        join: {
          from: "salon_address.salon_id",
          to: "salon_authentication.id"
        }
      },
      email: {
        relation: Model.HasManyRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/SalonEmail",
        join: {
          from: "salon_email.salon_id",
          to: "salon_authentication.id"
        }
      },
      phone: {
        relation: Model.HasManyRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/SalonPhone",
        join: {
          from: "salon_phone.salon_id",
          to: "salon_authentication.id"
        }
      },
      payments: {
        relation: Model.HasManyRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/SalonPayments",
        join: {
          from: "salon_payment_details.salon_id",
          to: "salon_authentication.id"
        }
      },
      kycDetails: {
        relation: Model.HasManyRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/SalonKYCDetails",
        join: {
          from: "salon_kyc_details.salon_id",
          to: "salon_authentication.id"
        }
      },
      professional: {
        relation: Model.HasManyRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/Professional",
        join: {
          from: "professional.salon_id",
          to: "salon_authentication.id"
        }
      },
      salon_services_map: {
        relation: Model.HasManyRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/SalonServicesMap",
        join: {
          from: "salon_services_map.salon_id",
          to: "salon_authentication.id"
        }
      },
      user_credits: {
        relation: Model.HasManyRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/UserCredits",
        join: {
          from: "userCredits.salon_id",
          to: "salon_authentication.id"
        }
      },
      bookedslots: {
        relation: Model.HasManyRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/BookedSlots",
        join: {
          from: "bookedslots.salon_id",
          to: "salon_authentication.id"
        }
      }
    };
  }
}
module.exports = Salon;
