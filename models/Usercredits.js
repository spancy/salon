const Knex = require("knex");
const connection = require("../knexfile");
const { Model } = require("objection");

const knexConnection = Knex(connection.development);

Model.knex(knexConnection);
//Adding modelx
class Usercredits extends Model {
  static get tableName() {
    return "userCredits";
  }

  static get relationMappings() {
    return {
      employee: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + "/salon",
        join: {
          from: "userCredits.salon_id",
          to: "salon_authentication.id"
        }
      },
      interest: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + "/Credits",
        join: {
          from: "userCredits.credit_id",
          to: "credits.id"
        }
      }
    };
  }
}

module.exports = Usercredits;
