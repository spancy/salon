"use strict";

const Model = require("objection").Model;
const knex = require("knex");

const KnexConfig = require("../knexfile");

Model.knex(knex(KnexConfig.development));
class BookedSlots extends Model {
  // Table name is the only required property.
  static get tableName() {
    return "bookedslots";
  }

  static get relationMappings() {
    return {
      salon: {
        relation: Model.BelongsToOneRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/Salon",
        join: {
          from: "bookedslots.salon_id",
          to: "salon_authentication.id"
        }
      }
    };
  }
}
module.exports = BookedSlots;
