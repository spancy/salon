var { QueryBuilder } = require("objection");
var salon = require("../models/Salon");
var salonProfile = require("../models/SalonProfile");
var salonAddress = require("../models/SalonAddress");
var salonEmail = require("../models/SalonEmail");
var salonKYCDetails = require("../models/SalonKYCDetails");
var salonPayments = require("../models/SalonPayments");
var salonPhone = require("../models/SalonPhone");
var credits = require("../models/Credits");
var Referrals = require("../models/Referrals");
var UserReferrals = require("../models/UserReferrals");
var usercredits = require("../models/Usercredits");
var bookedslots = require("../models/BookedSlots");
var salon_services_map = require("../models/SalonServicesMap");
var services = require("../models/Services");
var professional = require("../models/Professional");
var professionalServiceMap = require("../models/ProfessionalServiceMap");
const config = require("../config");
const axios = require("axios");
var mailer = require("nodemailer-promise");
const nodemailer = require("nodemailer");
const gigsta_config = require("../gigstaconfig");
var helper = require("./helper");

//function to get all salons by location(Graph Select)
async function salonGetAllData(req) {
  try {
    var salonAllData = await salon
      .query()
      .eager(
        "[profile,email,address,kycDetails,payments,professional,salon_services_map,bookedslots]"
      );

    let message = "";

    salonAllData.map((el, i) => {
      el["distance"] =
        el.profile === null
          ? ""
          : helper.calcCrow(
              el.profile.lat,
              el.profile.lon,
              req.query.lat,
              req.query.lon
            );
      return el;
    });
    salonAllData = salonAllData.filter((el, i) => {
      return el.distance <= config.MAX_DISTANCE_RADIUS && el.distance !== "";
    });

    message = {
      statusCode: 200,
      body: salonAllData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all salons by Service and location(Graph Select)
async function salonGetAllDataByService(req) {
  try {
    var salonAllData;
    if (req.query.serviceID === undefined) {
      salonAllData = await salon
        .query()
        .withGraphJoined(
          "[profile,email,address,kycDetails,payments,professional,salon_services_map,bookedslots]"
        );
    } else {
      salonAllData = await salon
        .query()
        .withGraphJoined(
          "[profile,email,address,kycDetails,payments,professional,salon_services_map,bookedslots]"
        )
        .where("salon_services_map.service_id", req.query.serviceID);
    }

    let message = "";
    salonAllData.map((el, i) => {
      el["distance"] =
        el.profile === null
          ? ""
          : helper.calcCrow(
              el.profile.lat,
              el.profile.lon,
              req.query.lat,
              req.query.lon
            );
      return el;
    });
    /*  salonAllData = salonAllData.filter((el, i) => {
      return el.distance <= config.MAX_DISTANCE_RADIUS && el.distance !== "";
    });
*/
    message = {
      statusCode: 200,
      body: salonAllData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all salons by location(Graph Select)
async function salonGetAllDataByLocation(req) {
  try {
    var salonAllData = await salon
      .query()
      .withGraphJoined(
        "[profile,email,address,kycDetails,payments,professional,salon_services_map,bookedslots]"
      );
    let message = "";
    salonAllData.map((el, i) => {
      el["distance"] =
        el.profile === null
          ? ""
          : helper.calcCrow(
              el.profile.lat,
              el.profile.lon,
              req.query.lat,
              req.query.lon
            );
      return el;
    });
    /*  salonAllData = salonAllData.filter((el, i) => {
      return el.distance <= config.MAX_DISTANCE_RADIUS && el.distance !== "";
    });
*/
    message = {
      statusCode: 200,
      body: salonAllData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all templates and its underlying relations
async function salonGetData(req, queryBy) {
  try {
    const salonData = await salon
      .query()
      .where(queryBy, req.params[queryBy])
      .eager(
        "[profile,email,address,kycDetails,payments,professional,salon_services_map,bookedslots]"
      );

    let message = "";

    message = {
      statusCode: 200,
      body: salonData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all templates and its underlying relations
async function salonGetAddressData(req, queryBy) {
  try {
    const salonAddressData = await salonAddress
      .query()
      .where(queryBy, req.params.id);

    let message = "";

    message = {
      statusCode: 200,
      body: salonAddressData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all templates and its underlying relations
async function salonGetEmailData(req, queryBy) {
  try {
    const salonEmailData = await salonEmail
      .query()
      .where(queryBy, req.params.id);

    let message = "";

    message = {
      statusCode: 200,
      body: salonEmailData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all templates and its underlying relations
async function salonGetProfileData(req, queryBy) {
  try {
    const salonProfileData = await salonProfile
      .query()
      .where(queryBy, req.params.id);

    let message = "";

    message = {
      statusCode: 200,
      body: salonProfileData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all templates and its underlying relations
async function salonGetKYCData(req, queryBy) {
  try {
    const salonKYCData = await salonKYCDetails
      .query()
      .where(queryBy, req.params.id);

    let message = "";

    message = {
      statusCode: 200,
      body: salonKYCData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all templates and its underlying relations
async function salonGetPaymentsData(req, queryBy) {
  try {
    const salonPaymentsData = await salonPayments
      .query()
      .where(queryBy, req.params[queryBy])
      .orderBy("created_at", "desc");

    let message = "";

    message = {
      statusCode: 200,
      body: salonPaymentsData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function GetPaymentsDataBysalonID(req, queryBy) {
  try {
    const GetPaymentsDataBysalonID = await salonPayments
      .query()
      .where(queryBy, req.params.salon_id)
      .orderBy("created_at", "desc");

    let message = "";

    message = {
      statusCode: 200,
      body: salonPaymentsData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

/*------------------------------ Credits -----------------------------------*/

//function to get all credits and their relations
async function saloncreditsAllData(req) {
  try {
    const saloncreditsAllData = await credits.query();

    let message = "";

    message = {
      statusCode: 200,
      body: saloncreditsAllData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function salonreferralsAllData(req) {
  try {
    const salonreferralsAllData = await Referrals.query();

    let message = "";

    message = {
      statusCode: 200,
      body: salonreferralsAllData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get a address and its underlying relations
async function salonGetCreditsData(req, queryBy) {
  try {
    const salonGetCreditsData = await credits
      .query()
      .where(queryBy, req.params[queryBy]);

    let message = "";

    message = {
      statusCode: 200,
      body: salonGetCreditsData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function salonGetUserCreditsData(req, queryBy) {
  try {
    var today = new Date();
    today.toUTCString();
    var todate =
      today.getFullYear() +
      "/" +
      (today.getMonth() + 1) +
      "/" +
      today.getDate();
    //console.log(today);
    const salonGetUserCreditsData = await usercredits
      .query()
      .where(queryBy, req.params[queryBy])
      //.where('expiration_date' > raw(now()))
      .orderBy("expiration_date", "asc");

    let message = "";

    message = {
      statusCode: 200,
      body: salonGetUserCreditsData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function salonGetReferralsData(req, queryBy) {
  try {
    const salonGetReferralsData = await Referrals.query().where(
      queryBy,
      req.params[queryBy]
    );

    let message = "";

    message = {
      statusCode: 200,
      body: salonGetReferralsData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function salonGetUserReferralsData(req, queryBy) {
  try {
    const salonGetUserReferralsData = await UserReferrals.query().where(
      queryBy,
      req.params[queryBy]
    );

    let message = "";

    message = {
      statusCode: 200,
      body: salonGetUserReferralsData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all templates and its underlying relations
async function salonGetPhoneData(req, queryBy) {
  try {
    const salonPhoneData = await salonPhone
      .query()
      .where(queryBy, req.params.id);

    let message = "";

    message = {
      statusCode: 200,
      body: salonPhoneData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//GET Servcies data

async function salonServicesAllData(req, queryBy) {
  try {
    const servicesData = await services.query();

    let message = "";

    message = {
      statusCode: 200,
      body: servicesData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function salonGetServicesData(req, queryBy) {
  try {
    const servicesData = await services.query().where(queryBy, req.params.id);

    let message = "";

    message = {
      statusCode: 200,
      body: servicesData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//GET Professional Data
async function getProfessionalAllData(req, queryBy) {
  try {
    const professionalData = await professional.query();

    let message = "";

    message = {
      statusCode: 200,
      body: professionalData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function getProfessionalData(req, queryBy) {
  try {
    const professionalData = await professional
      .query()
      .where(queryBy, req.params[queryBy]);

    let message = "";

    message = {
      statusCode: 200,
      body: professionalData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//get available professionals for a booked slot
async function getAvailableProfessionals(req) {
  try {
    console.log("Query parameters" , req.query);
    var professionalRecords = await professional
      .query()
      .where("salon_id", req.query.salonID)
      .whereNotExists(function() {
        this.select(1)
          .from("bookedslots")
          .whereNotNull("professional_id")
          .andWhere("salon_id", req.query.salonID)
          .where("date_of_booking", req.query.bookingDate)
          .andWhereRaw("bookedslots.professional_id = professional.id")
          .andWhereRaw(
            ' (( "startTime" between ? and  ? ) \
                  or ( "endTime" between ? and  ? ) )',
            [
              req.query.startTime,
              req.query.endTime,
              req.query.startTime,
              req.query.endTime
            ]
          );
      })
      .whereExists(function(){
        this.select(1)
        .from("professional_services_map")
        .where("service_id", req.query.serviceID)
        .andWhereRaw("professional_services_map.professional_id = professional.id")
      });

    let message = "";

    message = {
      statusCode: 200,
      body: professionalRecords
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//GET SalonServicesMap data
async function salonServicesMapAllData(req, queryBy) {
  try {
    const servicesMapData = await salon_services_map.query();

    let message = "";

    message = {
      statusCode: 200,
      body: servicesMapData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function salonServicesMapData(req, queryBy) {
  try {
    const servicesData = await salon_services_map
      .query()
      .where(queryBy, req.params[queryBy]);

    let message = "";

    message = {
      statusCode: 200,
      body: servicesData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function bookedSlots(req) {
  try {
    const bookedslotsData = await bookedslots
      .query()
      .where("salon_id", req.query.salonID)
      .where("date_of_booking", req.query.bookingDate);

    let message = "";

    message = {
      statusCode: 200,
      body: bookedslotsData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//GET Booked Slots data
async function bookedSlotsAllData(req, queryBy) {
  try {
    const bookedslotsData = await bookedslots.query();

    let message = "";

    message = {
      statusCode: 200,
      body: bookedslotsData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function bookedSlotsData(queryBy, req) {
  try {
    const servicesData = await bookedslots
      .query()
      .where(queryBy, req.params[queryBy]);

    let message = "";

    message = {
      statusCode: 200,
      body: servicesData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function professionalServicesMapData(req,queryBy) {
  try {
    const servicesData = await professionalServiceMap
      .query()
      .where(queryBy, req.params[queryBy]);

    let message = "";

    message = {
      statusCode: 200,
      body: servicesData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

module.exports.salonGetAllData = salonGetAllData;
module.exports.salonGetData = salonGetData;
module.exports.salonGetProfileData = salonGetProfileData;
module.exports.salonGetPhoneData = salonGetPhoneData;
module.exports.salonGetPaymentsData = salonGetPaymentsData;
module.exports.salonGetKYCData = salonGetKYCData;
module.exports.salonGetEmailData = salonGetEmailData;
module.exports.salonGetAddressData = salonGetAddressData;
module.exports.saloncreditsAllData = saloncreditsAllData;
module.exports.salonGetCreditsData = salonGetCreditsData;
module.exports.salonGetUserCreditsData = salonGetUserCreditsData;
module.exports.salonGetUserReferralsData = salonGetUserReferralsData;
module.exports.salonGetReferralsData = salonGetReferralsData;
module.exports.salonreferralsAllData = salonreferralsAllData;
module.exports.bookedSlotsData = bookedSlotsData;
module.exports.bookedSlotsAllData = bookedSlotsAllData;
module.exports.salonServicesMapData = salonServicesMapData;
module.exports.salonServicesMapAllData = salonServicesMapAllData;
module.exports.getProfessionalData = getProfessionalData;
module.exports.getProfessionalAllData = getProfessionalAllData;
module.exports.salonGetServicesData = salonGetServicesData;
module.exports.salonServicesAllData = salonServicesAllData;
module.exports.professionalServicesMapData = professionalServicesMapData;
module.exports.bookedSlots = bookedSlots;
module.exports.salonGetAllDataByService = salonGetAllDataByService;
module.exports.salonGetAllDataByLocation = salonGetAllDataByLocation;
module.exports.getAvailableProfessionals = getAvailableProfessionals;
