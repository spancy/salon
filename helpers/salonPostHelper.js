var { QueryBuilder } = require("objection");
var salon = require("../models/Salon");
var salonProfile = require("../models/SalonProfile");
var salonAddress = require("../models/SalonAddress");
var salonEmail = require("../models/SalonEmail");
var salonKYCDetails = require("../models/SalonKYCDetails");
var salonPayments = require("../models/SalonPayments");
var salonPhone = require("../models/SalonPhone");
var credits = require("../models/Credits");
var Referrals = require("../models/Referrals");
var UserReferrals = require("../models/UserReferrals");
var usercredits = require("../models/Usercredits");
var bookedSlots = require("../models/BookedSlots");
var salon_services_map = require("../models/SalonServicesMap");
var services = require("../models/Services");
var professional = require("../models/Professional");
var professionalServiceMap = require("../models/ProfessionalServiceMap");
const config = require("../config");
const axios = require("axios");
var mailer = require("nodemailer-promise");
const nodemailer = require("nodemailer");
const gigsta_config = require("../gigstaconfig");

//function to insert into template table and relations.
async function salonInsert(req) {
  const salonData = {
    is_individual: req.body.is_individual,
    user_name: req.body.user_name,
    password: req.body.password,
    registered_email: req.body.registered_email,
    phone_number: req.body.phone_number,
    is_otp_verified: req.body.is_otp_verified,
    is_email_verified: req.body.is_email_verified,
    email_verification_hash: req.body.email_verification_hash,
    otp_hash: req.body.otp_hash,
    reset_password_hash: req.body.reset_password_hash,
    password_last_modified: req.body.password_last_modified,
    is_active: req.body.is_active,
   // profile: req.body.profile,
    //profile: {"is_kyc_verified": "true"},
    address: req.body.address,
    email: req.body.email,
    kycDetails: req.body.kycDetails,
    payments: req.body.payments
  };
   var salonProfile = {
     body:{
       salon_id: ""
      }
     }
  try {
    let message = "";
    const salonResult = await salon.query().insertGraph(salonData);
    //await salonProfile.relatedQuery('salon').relate(salonResult);

    message = {
      statusCode: 200,
      body: {
        message: "New salon Created",
        id: salonResult.id
      }
    };
    salonProfile.body.salon_id = salonResult.id;
    await salonProfileInsert(salonProfile);
    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New salon Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function salonProfileInsert(req) {
  const salonProfileData = {
    salon_id: req.body.salon_id,
    primary_address_id: req.body.primary_address_id,
    default_shipping_address_id: req.body.default_shipping_address_id,
    display_image_url: req.body.display_image_url,
    primary_phone_id: req.body.primary_phone_id,
    primary_email_id: req.body.primary_email_id,
    is_individual: req.body.is_individual,
    salon_type: req.body.salon_type,
    is_account_verified: req.body.is_account_verified,
    is_kyc_verified: req.body.is_kyc_verified,
    is_email_verified: req.body.is_active,
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    date_of_birth: req.body.date_of_establishment,
    user_name: req.body.user_name,
    orgnaization_name: req.body.orgnaization_name,
    is_active: req.body.is_active,
    startTime: req.body.StartTime,
    endTime: req.body.EndTime,
    breakStartTime: req.body.breakStartTime,
    breakEndTime: req.body.breakEndTime,
    weekly_holiday: req.body.weekly_holiday,
    lat: req.body.lat,
    lon: req.body.lon
  };

  try {
    let message = "";
    const salonProfile1 = await salonProfile
      .query()
      .insertGraph(salonProfileData);

    message = {
      statusCode: 200,
      body: {
        message: "New salon Profile Created",
        id: salonProfile1.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New salon Profile Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function razorpayPostContact(req) {
  const axios = require("axios");
  const key = config.key;
  const secret = config.secret;
  //JSON.stringify(payload);

  const payload = {
    name: req.body.name,
    email: req.body.email,
    contact: req.body.contact,
    type: req.body.type
  };

  try {
    var options = {
      method: "POST",
      url:
        "https://" + key + ":" + secret + "@api.razorpay.com" + "/v1/contacts",
      data: payload
    };
    //console.log(options);
    const response = await axios(options);
    console.log("Response is " + response.data);
    return response.data;
  } catch (err) {
    return err;
  }
}
//End of Contact API call

//Call to Razorpay Fund Account API to create a fund account for a contact
async function razorpayPostFundAccBank(req) {
  const axios = require("axios");
  const key = config.key;
  const secret = config.secret;
  //JSON.stringify(payload);

  const payload = {
    contact_id: req.body.contact_id,
    account_type: req.body.account_type,
    bank_account: req.body.bank_account
  };

  try {
    var options = {
      method: "POST",
      url:
        "https://" +
        key +
        ":" +
        secret +
        "@api.razorpay.com" +
        "/v1/fund_accounts",
      data: payload
    };
    //console.log(options);
    const response = await axios(options);
    console.log("Response is " + response.data);
    return response.data;
  } catch (err) {
    console.log("Error Response is ", {
      status: err.response.status,
      statusText: err.response.statusText
    });
    return { status: err.response.status, statusText: err.response.statusText };
  }
}
//End of Fund account API call

//Call to Razorpay Fund Account API to create a fund account for a contact
async function razorpayPostFundAccVpa(req) {
  const axios = require("axios");
  const key = config.key;
  const secret = config.secret;
  //JSON.stringify(payload);

  const payload = {
    contact_id: req.body.contact_id,
    account_type: req.body.account_type,
    vpa: req.body.vpa
    //payment_id:req.body.payment_id
  };

  try {
    var options = {
      method: "POST",
      url:
        "https://" +
        key +
        ":" +
        secret +
        "@api.razorpay.com" +
        "/v1/fund_accounts",
      //url: 'https://'+key+':'+secret+'@api.razorpay.com'+'/v1/payments/'+payload.payment_id+'/refund'
      data: payload
    };
    //console.log(options);
    const response = await axios(options);
    console.log("Response is " + response.data);
    return response.data;
  } catch (err) {
    console.log("Error Response is ", {
      status: err.response.status,
      statusText: err.response.statusText
    });
    return { status: err.response.status, statusText: err.response.statusText };
  }
}
//End of Fund account API call

async function salonEmailInsert(req) {
  const salonEmailData = {
    salon_id: req.body.salon_id,
    email_address: req.body.emailAddress,
    description: req.body.description,
    verified: req.body.verified,
    reserved: req.body.reserved,
    email_comm_ok: req.body.email_comm_ok
  };

  try {
    let message = "";
    const salonEmail1 = await salonEmail.query().insertGraph(salonEmailData);

    message = {
      statusCode: 200,
      body: {
        message: "New salon Email Created",
        id: salonEmail1.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New salon Email Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function salonPhoneInsert(req) {
  const salonPhoneData = {
    salon_id: req.body.salon_id,
    number: req.body.number,
    country_code: req.body.country_code,
    last4: req.body.last4,
    type: req.body.type,
    verified: req.body.verified,
    reserved: req.body.reserved
  };

  try {
    let message = "";
    const salonPhone1 = await salonPhone.query().insertGraph(salonPhoneData);

    message = {
      statusCode: 200,
      body: {
        message: "New salon Phone Created",
        id: salonPhone1.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New salon Phone Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function salonAddressInsert(req) {
  const salonAddressData = {
    salon_id: req.body.salon_id,
    line1: req.body.line1,
    line2: req.body.line2,
    city: req.body.city,
    zip_code: req.body.zip_code,
    state: req.body.state,
    country: req.body.country
  };

  try {
    let message = "";
    const salonAddress1 = await salonAddress
      .query()
      .insertGraph(salonAddressData);

    message = {
      statusCode: 200,
      body: {
        message: "New salon Address Created",
        id: salonAddress1.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New salon Address Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function salonKYCDetailsInsert(req) {
  const salonKYCDetailsData = {
    salon_id: req.body.salon_id,
    document_type: req.body.document_type,
    document_unique_id: req.body.document_unique_id,
    description: req.body.description,
    verified: req.body.verified,
    document_id: req.body.document_id
  };

  try {
    let message = "";
    const salonKYCDetails1 = await salonKYCDetails
      .query()
      .insertGraph(salonKYCDetailsData);

    message = {
      statusCode: 200,
      body: {
        message: "New salon KYC Details Created",
        id: salonKYCDetails1.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New salon KYC Details Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function creditsInsert(req) {
  const creditsInsertData = {
    name: req.body.name,
    description: req.body.description,
    maxCredit: req.body.maxCredit,
    cost: req.body.cost,
    expirationPolicy: req.body.expirationPolicy
  };

  try {
    let message = "";
    const instance = await credits.query().insertGraph(creditsInsertData);

    message = {
      statusCode: 200,
      body: {
        message: "New credits Created",
        id: instance.id
      }
    };

    console.log(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New credits Errored" + e
    };
    console.log(message);
    return message;
  }
}

async function referralsInsert(req) {
  const referralsInsertData = {
    salon_id: req.body.salon_id,
    referral_code: req.body.referral_code
  };

  try {
    let message = "";
    const instance = await Referrals.query().insertGraph(referralsInsertData);

    message = {
      statusCode: 200,
      body: {
        message: "New referral Created",
        id: instance.id
      }
    };

    console.log(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New referral Errored" + e
    };
    console.log(message);
    return message;
  
  }
}

async function userCreditsInsert(req) {
  const userCreditsData = {
    salon_id: req.body.salon_id,
    credit_id: req.body.credit_id,
    activation_date: req.body.activation_date,
    initial_credits: req.body.initial_credits,
    expiration_date: req.body.expiration_date,
    remaining_credits: req.body.remaining_credits,
    spent_credits: req.body.spent_credits
  };

  try {
    let message = "";
    const userCredits = await usercredits.query().insertGraph(userCreditsData);

    message = {
      statusCode: 200,
      body: {
        message: "New userCredits Created",
        id: userCredits.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New userCredits Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function sendEmailInvoice(req) {
  //console.log("Email data",emailData);
  const gigsta_config = require("../gigstaconfig");
  const emailData = {
    salon_id: req.body.salon_id,
    salon_name: req.body.salon_name,
    employee_id: req.body.employee_id,
    employee_name: req.body.employee_name,
    task_id: req.body.task_id,
    task_name: req.body.task_name,
    case_no: req.body.case_no,
    employee_email: req.body.employee_email,
    task_charges: req.body.task_charges
  };

  console.log("Email data", emailData);
  try {
    let message = "";
    var nodemailer = require("nodemailer");

    var transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: gigsta_config.senderMail,
        pass: gigsta_config.senderMailPassword
      }
    });

    var mailOptions = {
      from: gigsta_config.senderMail,
      to: emailData.employee_email,
      subject: "Gigsta - Invoice",
      html:
        "<!DOCTYPE html><html>\
          <body>\
          <p>Sender name : " +
        emailData.salon_name +
        "</p>\
          </br>\
          <p>Task name : " +
        emailData.task_name +
        "</p>\
          </br>\
          <p>Case No : " +
        emailData.case_no +
        "</p>\
          </br>\
          <p>Charges : " +
        emailData.task_charges +
        "</p>\
          </br>\
          <p>Thank you for the task. This is an invoice for the task provided. Please pay Rs." +
        emailData.task_charges +
        "</p>\
          </br>\
          </body>\
          </html>"
    };

    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info.response);
      }
    });
    /*let message = "";
    var sendEmail = mailer.config({
        email: gigsta_config.senderMail,
        password: gigsta_config.senderMailPassword,
        server: gigsta_config.senderServer
    });
//console.log("sendEmail",sendEmail);
    var options = {
        subject: 'Gigsta - Invoice',
        senderName: emailData.salon_name,
        receiver: emailData.employee_email,
        html: '<!DOCTYPE html><html>\
        <body>\
        <p>Task name : '+emailData.task_name+'</p>\
        </br>\
        <p>Case No : '+emailData.case_no+'</p>\
        </br>\
        <p>Charges : '+emailData.task_charges+'</p>\
        </br>\
        <p>Thank you for the task. This is an invoice for the task provided. Please pay Rs.'+emailData.task_charges+'</p>\
        </br>\
        </body>\
        </html>'
    };


    message = {
      statusCode: 200,
      body: {
        message: "Email sent"
      }
    };

    console.log(message);
    //  res.json(message);
    return message;*/
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Email Errored" + e
    };
  }
  message = {
    statusCode: 200,
    body: {
      message: "Email sent"
    }
  };
  return message;
}

async function userReferralsInsert(req) {
  const { raw } = require("objection");
  const userReferralsData = {
    referral_id: req.body.referral_id,
    referred_to: req.body.referred_to
  };
  current_date = new Date();
  try {
    let message = "";
    const userReferrals = await UserReferrals.query().insertGraph(
      userReferralsData
    );

    /*const salonGetReferralsData = await Referrals
      .query()
      .where('id', userReferralsData.referral_id);

     const referrer_id = salonGetReferralsData[0].salon_id;
//console.log("1. Referrer id",referrer_id);
     const creditsGetData = await credits
        .query()
        .where('id', 4);
        expiration_policy = creditsGetData[0].expirationPolicy;
        //console.log("2. Expiration policy",expiration_policy);
//Referree Credits Code
    const getCredits_referree = await usercredits.query()
     .where('salon_id',userReferralsData.referred_to)
     .where('credit_id', 4);
//console.log("3. getCredits_referree is null or not",getCredits_referree);
     /// Code for POST ot PUT to userCredits for referred_to
    if(getCredits_referree.length > 0)
    {
      //console.log("4. getCredits_referree is  not null");
      const userCredits_referree = await usercredits.query()
      .patch({
        initial_credits: getCredits_referree[0].remaining_credits,
        remaining_credits:raw('remaining_credits+5')
      })
      .where('salon_id',userReferralsData.referred_to)
      .where('credit_id', 4);
    }
    else{
      //console.log("4. getCredits_referree is null");
    const userCreditsRefereeData = {
      salon_id: userReferralsData.referred_to,
      credit_id: 4,
      activation_date: new Date(),
      initial_credits: 0,
      expiration_date: new Date(current_date.setMonth(current_date.getMonth() + creditsGetData[0].expirationPolicy)),
      remaining_credits: creditsGetData[0].maxCredit,
      spent_credits: 0
    };
    //console.log("5. userCreditsRefereeData is this",userCreditsRefereeData);
    const userCreditsReferee = await usercredits.query().insertGraph(userCreditsRefereeData);
    //console.log("6. userCreditsReferee is inserted",userCreditsReferee);
  }
//Referrer Credits code
    const getCredits_referrer = await usercredits.query()
    .where('salon_id',referrer_id)
    .where('credit_id', 4);
//console.log("7. getCredits_referrer is null or not",getCredits_referrer);
//console.log(" Length of getCredits_referrer", getCredits_referrer.length);
    //if(Object.getOwnPropertyNames(getCredits_referrer).length > 0)
    if(getCredits_referrer.length > 0)
    {
    //  console.log("8. getCredits_referrer is  not null");
      const userCredits_referrer = await usercredits.query()
      .patch({
        initial_credits: getCredits_referrer[0].remaining_credits,
        remaining_credits:raw('remaining_credits+5')
      })
      .where('salon_id',referrer_id)
      .where('credit_id', 4);
    }
    else{
      //console.log("9. getCredits_referrer is null");
    const userCreditsReferrerData = {
      salon_id: referrer_id,
      credit_id: 4,
      activation_date: new Date(),
      initial_credits: 0,
      expiration_date: new Date(current_date.setMonth(current_date.getMonth() + creditsGetData[0].expirationPolicy)),
      remaining_credits: creditsGetData[0].maxCredit,
      spent_credits: 0
    };
  //  console.log("10. userCreditsRefererData is this",userCreditsReferrerData);
    const userCreditsReferrer = await usercredits.query().insertGraph(userCreditsReferrerData);
  //  console.log("6. userCreditsReferer is inserted",userCreditsReferrer);
}*/

    message = {
      statusCode: 200,
      body: {
        message: "New userReferrals Created",
        id: userReferrals.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New userReferrals Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function referralGenerator(req) {
  var firstPart = (Math.random() * 46656) | 0;
  var secondPart = (Math.random() * 46656) | 0;
  firstPart = ("000" + firstPart.toString(36)).slice(-3);
  secondPart = ("000" + secondPart.toString(36)).slice(-3);
  return firstPart + secondPart;
}

async function salonPaymentsInsert(req) {
  const salonPaymentsData = {
    salon_id: req.body.salon_id,
    payment_type: req.body.payment_type,
    payment_unique_id: req.body.payment_unique_id,
    verified: req.body.verified,
    description: req.body.description,
    document_id: req.body.document_id,
    fund_account_id: req.body.fund_account_id
  };

  try {
    let message = "";
    const salonPayments1 = await salonPayments
      .query()
      .insertGraph(salonPaymentsData);

    message = {
      statusCode: 200,
      body: {
        message: "New salon Payments Created",
        id: salonPayments1.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New salon Payments  Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//Insert into services table
async function servicesInsert(req) {
  const servicesData = {
    service_name: req.body.service_name,
    service_display_image_url: req.body.service_display_image_url,
    is_combo: req.body.is_combo,
    duration: req.body.duration,
    category: req.body.category
  };

  try {
    let message = "";
    const services1 = await services.query().insertGraph(servicesData);

    message = {
      statusCode: 200,
      body: {
        message: "New services Created",
        id: services1.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New services Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//Insert into Professional table
async function professionalInsert(req) {
  const professionalData = {
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    display_image_url: req.body.display_image_url,
    is_bank_verified: req.body.is_bank_verified,
    is_kyc_verified: req.body.is_kyc_verified,
    salon_id: req.body.salon_id,
    phone_number: req.body.phone_number,
    list_of_service_ids: req.body.list_of_service_ids
  };

  try {
    let message = "";
    const newprofessional = await professional
      .query()
      .insertGraph(professionalData);

    message = {
      statusCode: 200,
      body: {
        message: "New salon Professional Created",
        id: newprofessional.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New salon Professional  Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//Insert into bookedslots table
async function bookedslotsInsert(req) {
  var bookedslotsData = {
     salon_id: parseInt(req.body.salon_id),
    professional_id: req.body.professional_id,
    date_of_booking: req.body.date_of_booking,
    startTime: parseInt(req.body.startTime),
    endTime: parseInt(req.body.endTime),
    is_booked: req.body.is_booked,
    is_unavailable: req.body.is_unavailable,
    is_salon_holiday: req.body.is_salon_holiday
  };

  var isRandomProfessional = req.body.is_random_professional;

  try {
    if (isRandomProfessional) {
      var professionalRecord = await professional
        .query()
        .where("salon_id", bookedslotsData.salon_id)
        .whereNotExists(function() {
          this.select(1)
            .from("bookedslots")
            .whereNotNull("professional_id")
            .andWhere("salon_id", bookedslotsData.salon_id)
            .andWhereRaw("bookedslots.professional_id = professional.id")
            .andWhereRaw(
              ' (( "startTime" between ? and  ? ) \
                    or ( "endTime" between ? and  ? ) )',
              [
                bookedslotsData.startTime,
                bookedslotsData.endTime,
                bookedslotsData.startTime,
                bookedslotsData.endTime
              ]
            );
        })
        .select("id")
        .first();
      if (professionalRecord !== undefined)
        bookedslotsData.professional_id = professionalRecord.id;
    }
    let message = "";
    const bookedslots1 = await bookedSlots.query().insertGraph(bookedslotsData);

    message = {
      statusCode: 200,
      body: {
        message: "New Booked Slots Created",
        id: bookedslots1.id + "-" + bookedslots1.professional_id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New Booked Slots Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//Insert into salon_services_map table

async function salonServicesMapInsert(req) {
  const salonServicesMapData = {
    rate: req.body.rate,
    athome_additional_rate: req.body.athome_additional_rate,
    is_athome_offered: req.body.is_athome_offered,
    currency: req.body.currency,
    salon_id: req.body.salon_id,
    service_id: req.body.service_id,
    duration: req.body.duration
  };

  try {
    let message = "";
    const salonservicesmap = await salon_services_map
      .query()
      .insertGraph(salonServicesMapData);

    message = {
      statusCode: 200,
      body: {
        message: "New salonservicesmap Created",
        id: salonservicesmap.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New salonservicesmap Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//Insert professionalservice map insert

async function professionalServicesMapInsert(req) {
  const professionalServicesMapData = {
    professional_id: req.body.professional_id,
    salon_services_map_id: req.body.salon_services_map_id,
    service_id: req.body.service_id
  };

  try {
    let message = "";
    const salonservicesmap = await professionalServiceMap
      .query()
      .insertGraph(professionalServicesMapData);

    message = {
      statusCode: 200,
      body: {
        message: "New professionalServiceMap Created",
        id: salonservicesmap.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New professionalServiceMap Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//Send Invoice email function

async function sendInvoiceSes(req) {
  const gigstaconfig = require("../gigstaconfig");

  const emailData = {
    salon_id: req.body.salon_id,
    salon_name: req.body.salon_name,
    employee_id: req.body.employee_id,
    employee_name: req.body.employee_name,
    task_id: req.body.task_id,
    task_name: req.body.task_name,
    case_no: req.body.case_no,
    employee_email: req.body.employee_email,
    task_charges: req.body.task_charges
  };

  const AWS = require("aws-sdk");

  // Amazon SES configuration
  const SESConfig = {
    apiVersion: "2010-12-01",
    accessKeyId: config.accessKeyId,
    secretAccessKey: config.secretAccessKey,
    region: config.region
  };
  console.log("Ses config", SESConfig);
  //  console.log("sendMail",gigstaconfig);

  var params = {
    Source: gigsta_config.senderMail,
    Destination: {
      ToAddresses: [emailData.employee_email]
    },
    ReplyToAddresses: [gigstaconfig.sendMail],
    Message: {
      Body: {
        Html: {
          Charset: "UTF-8",
          Data:
            "<html>\
          <body>\
          <p>Sender name : " +
            emailData.salon_name +
            "</p>\
          </br>\
          <p>Task name : " +
            emailData.task_name +
            "</p>\
          </br>\
          <p>Case No : " +
            emailData.case_no +
            "</p>\
          </br>\
          <p>Charges : " +
            emailData.task_charges +
            "</p>\
          </br>\
          <p>Thank you for the task. This is an invoice for the task provided. Please pay Rs." +
            emailData.task_charges +
            "</p>\
          </br>\
          </body>\
          </html>"
        }
      },
      Subject: {
        Charset: "UTF-8",
        Data: "Gigsta - Invoice"
      }
    }
  };

  var sendPromise = new AWS.SES(SESConfig).sendEmail(params).promise();

  sendPromise
    .then(function(data) {
      console.log(data.MessageId);
    })
    .catch(function(err) {
      console.error(err, err.stack);
    });
}

module.exports.salonInsert = salonInsert;
module.exports.razorpayPostContact = razorpayPostContact;
module.exports.razorpayPostFundAccBank = razorpayPostFundAccBank;
module.exports.razorpayPostFundAccVpa = razorpayPostFundAccVpa;
module.exports.salonProfileInsert = salonProfileInsert;
module.exports.salonPaymentsInsert = salonPaymentsInsert;
module.exports.salonKYCDetailsInsert = salonKYCDetailsInsert;
module.exports.salonAddressInsert = salonAddressInsert;
module.exports.salonPhoneInsert = salonPhoneInsert;
module.exports.creditsInsert = creditsInsert;
module.exports.userCreditsInsert = userCreditsInsert;
module.exports.salonEmailInsert = salonEmailInsert;
module.exports.userReferralsInsert = userReferralsInsert;
module.exports.referralsInsert = referralsInsert;
module.exports.referralGenerator = referralGenerator;
module.exports.sendEmailInvoice = sendEmailInvoice;
module.exports.sendInvoiceSes = sendInvoiceSes;
module.exports.servicesInsert = servicesInsert;
module.exports.professionalInsert = professionalInsert;
module.exports.salonServicesMapInsert = salonServicesMapInsert;
module.exports.bookedslotsInsert = bookedslotsInsert;   
module.exports.professionalServicesMapInsert = professionalServicesMapInsert; 