var { QueryBuilder } = require("objection");
var salon = require("../models/Salon");
var salonProfile = require("../models/SalonProfile");
var salonAddress = require("../models/SalonAddress");
var salonEmail = require("../models/SalonEmail");
var salonKYCDetails = require("../models/SalonKYCDetails");
var salonPayments = require("../models/SalonPayments");
var salonPhone = require("../models/SalonPhone");
var credits = require("../models/Credits");
var Referrals = require("../models/Referrals");
var UserReferrals = require("../models/UserReferrals");
var usercredits = require("../models/Usercredits");
var bookedslots = require("../models/BookedSlots");
var salon_services_map = require("../models/SalonServicesMap");
var services = require("../models/Services");
var professional = require("../models/Professional");
var professionalServiceMap = require("../models/ProfessionalServiceMap");
const config = require("../config");
const axios = require("axios");
var mailer = require("nodemailer-promise");
const nodemailer = require("nodemailer");
const gigsta_config = require("../gigstaconfig");

//function to Update into template table and relations.
async function salonUpdate(req) {
  try {
    let message = "";
    const salonData = await salon
      .query()
      .update(req.body)
      .where("id", req.params.salon_id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated salon Details",
        id: salonData.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated salon Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function salonProfilePut(req) {
  try {
    let message = "";
    const salonProfile1 = await salonProfile
      .query()
      .update(req.body)
      .where("salon_id", req.params.salon_id);

    message = {
      statusCode: 200,
      body: {
        message: "salon Profile Updated",
        id: salonProfile1.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Update salon Profile Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function salonProfileUpdate(req) {
  try {
    let message = "";
    const salonProfile1 = await salonProfile
      .query()
      .update(req.body)
      .where("id", req.params.salon_profile_id);

    message = {
      statusCode: 200,
      body: {
        message: "salon Profile Updated",
        id: salonProfile1.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Update salon Profile Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//-------- Referrals-------
async function salonReferralsUpdate(req) {
  try {
    let message = "";
    const salonReferralsUpdate = await Referrals.query()
      .update(req.body)
      .where("id", req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "salon Referrals Updated",
        id: salonReferralsUpdate.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Update salon Referrals Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function salonReferralsUpdateBysalon(req) {
  try {
    let message = "";
    const salonReferralsUpdate = await Referrals.query()
      .update(req.body)
      .where("salon_id", req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "salon Referrals Updated",
        id: salonReferralsUpdate.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Update salon Referrals Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function salonUserReferralsUpdate(req) {
  try {
    let message = "";
    const salonUserReferralsUpdate = await UserReferrals.query()
      .update(req.body)
      .where("id", req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "salon UserReferrals Updated",
        id: salonUserReferralsUpdate.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Update salon UserReferrals Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function userReferralsUpdateByReferredTo(req) {
  try {
    let message = "";
    const salonUserReferralsUpdate = await UserReferrals.query()
      .update(req.body)
      .where("referred_to", req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "salon UserReferrals Updated",
        id: salonUserReferralsUpdate.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Update salon UserReferrals Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}
//----End of referrals----

async function razorpayPutContact(payload) {
  const axios = require("axios");
  const key = config.key;
  const secret = config.secret;
  //JSON.stringify(payload);

  payload = {
    name: req.body.name,
    email: req.body.email,
    contact: req.body.contact,
    type: req.body.type
  };

  try {
    var options = {
      method: "PATCH",
      url:
        "https://" + key + ":" + secret + "@api.razorpay.com" + "/v1/contacts",
      data: payload
    };
    console.log(options);
    const response = await axios(options);
    console.log(response.data);
    return response.data;
  } catch (err) {
    return err;
  }
}

async function salonEmailUpdate(req) {
  try {
    let message = "";
    const salonEmail1 = await salonEmail
      .query()
      .update(req.body)
      .where("id", req.params.salon_email_id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated salon Email ",
        id: salonEmail1.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated salon Email Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function salonPhoneUpdate(req) {
  try {
    let message = "";
    const salonPhone1 = await salonPhone
      .query()
      .update(req.body)
      .where("id", req.params.salon_phone_id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated salon Phone ",
        id: salonPhone1.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated salon Phone Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function salonAddressUpdate(req) {
  try {
    let message = "";
    const salonAddress1 = await salonAddress
      .query()
      .update(req.body)
      .where("id", req.params.salon_address_id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated salon Address ",
        id: salonAddress1.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated salon Address Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function salonKYCDetailsUpdate(req) {
  try {
    let message = "";
    const salonKYCDetails1 = await salonKYCDetails
      .query()
      .update(req.body)
      .where("id", req.params.salon_kyc_id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated salon KYC Details ",
        id: salonKYCDetails1.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated salon KYC Details Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function creditsUpdate(req) {
  try {
    let message = "";
    const creditsUpdate = await credits
      .query()
      .update(req.body)
      .where("id", req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated credits ",
        id: creditsUpdate.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated credits Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function userCreditsUpdate(req) {
  try {
    let message = "";
    const userCreditsUpdate = await usercredits
      .query()
      .update(req.body)
      .where("id", req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated userCredits ",
        id: userCreditsUpdate.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated userCredits Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function userCreditsUpdateBysalonID(req) {
  try {
    let message = "";
    const userCreditsUpdate = await usercredits
      .query()
      .update(req.body)
      .where("salon_id", req.params.salon_id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated userCredits Checkboxes ",
        id: userCreditsUpdate.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated userCredits Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function salonPaymentsUpdate(req) {
  try {
    let message = "";
    const salonPayments1 = await salonPayments
      .query()
      .update(req.body)
      .where("id", req.params.salon_payments_id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated salon Payments ",
        id: salonPayments.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated salon Payments  Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//Update Sevices
async function servicesUpdate(req,queryBy) {
  try {
    let message = "";
    const servicesUpdate = await services
      .query()
      .update(req.body)
      .where(queryBy, req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated Services ",
        id: servicesUpdate.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated Services Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//Update Professional
async function professionalUpdate(req,queryBy) {
  try {
    let message = "";
    const servicesUpdate = await professional
      .query()
      .update(req.body)
      .where(queryBy, req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated professional ",
        id: servicesUpdate.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated professional Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}
//Update bookedslots
async function bookedSlotsUpdate(req,queryBy) {
  try {
    let message = "";
    const servicesUpdate = await bookedslots
      .query()
      .update(req.body)
      .where(queryBy, req.params.id);

    message = {
      statusCode: 200,
      body: {
        message: "Updated bookedslots ",
        id: servicesUpdate.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated bookedslots Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}
//Update salon_services_map
async function salonServicesMapUpdate(req,salon_id,service_id) {
  try {
    let message = "";
    const servicesUpdate = await salon_services_map
      .query()
      .update(req.body)
      .where(salon_id, req.params[salon_id])
      .where(service_id, req.params[service_id]);

    message = {
      statusCode: 200,
      body: {
        message: "Updated ServicesMap ",
        id: servicesUpdate.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Updated ServicesMap Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function salonServicesMapDelete(req,salon_id,service_id) {
  try {
    let message = "";
    const servicesUpdate = await salon_services_map
      .query()
      .delete()
      .where(salon_id, req.params[salon_id])
      .where(service_id, req.params[service_id]);

    message = {
      statusCode: 200,
      body: {
        message: "Deleted ServicesMap ",
        id: servicesUpdate.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Delete ServicesMap Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function professionalDelete(req,salon_id,professional_id) {
  try {
    let message = "";
    const servicesUpdate = await professional
      .query()
      .delete()
      .where(salon_id, req.params[salon_id])
      .where('id', req.params[professional_id]);
      

    message = {
      statusCode: 200,
      body: {
        message: "Deleted Professional ",
        id: servicesUpdate.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Delete professional Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}
module.exports.salonUpdate = salonUpdate;
module.exports.salonProfilePut = salonProfilePut;
module.exports.razorpayPutContact = razorpayPutContact;
module.exports.salonProfileUpdate = salonProfileUpdate;
module.exports.salonPaymentsUpdate = salonPaymentsUpdate;
module.exports.salonKYCDetailsUpdate = salonKYCDetailsUpdate;
module.exports.salonAddressUpdate = salonAddressUpdate;
module.exports.salonPhoneUpdate = salonPhoneUpdate;
module.exports.salonEmailUpdate = salonEmailUpdate;
module.exports.userCreditsUpdateBysalonID = userCreditsUpdateBysalonID;
module.exports.userCreditsUpdate = userCreditsUpdate;
module.exports.creditsUpdate = creditsUpdate;
module.exports.salonUserReferralsUpdate = salonUserReferralsUpdate;
module.exports.salonReferralsUpdate = salonReferralsUpdate;
module.exports.salonReferralsUpdateBysalon = salonReferralsUpdateBysalon;
module.exports.userReferralsUpdateByReferredTo = userReferralsUpdateByReferredTo;
module.exports.bookedSlotsUpdate = bookedSlotsUpdate;
module.exports.servicesUpdate = servicesUpdate;
module.exports.professionalUpdate = professionalUpdate;
module.exports.salonServicesMapUpdate = salonServicesMapUpdate;
module.exports.salonServicesMapDelete = salonServicesMapDelete;
module.exports.professionalDelete = professionalDelete;