
exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable("professional_services_map", function(table) {
            table.increments("id").primary();
            table.integer("professional_id").references("professional.id");
            table.integer("service_id").references("services.id");
            table.integer("salon_services_map_id").references("salon_services_map.id");
            table.timestamps(true, true);
          })
        ]);
  };
  
  exports.down = function(knex, Promise) {};