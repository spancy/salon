// added lat lon
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable("salon_authentication", function(table) {
      table.increments("id").primary();
      table
        .boolean("is_individual")
        .notNullable()
        .defaultTo(true);
      table.string("user_name");
      table.string("password");
      table.string("registered_email");
      table.string("phone_number");
      table.boolean("is_otp_verified");
      table.boolean("is_email_verified");
      table.string("email_verification_hash");
      table.string("otp_hash");
      table.string("reset_password_hash");
      table.date("password_last_modified");
      table
        .boolean("is_active")
        .notNullable()
        .defaultTo(true);
      table.timestamps(true, true);
    }),

    knex.schema.createTable("salon_email", function(table) {
      table.increments("id").primary();
      table
        .integer("salon_id")
        .unsigned()
        .references("salon_authentication.id");
      table.string("description");
      table.string("email_address");
      table
        .boolean("verified")
        .notNullable()
        .defaultTo(false);
      table
        .boolean("reserved")
        .notNullable()
        .defaultTo(false);
      table
        .boolean("email_comm_ok")
        .notNullable()
        .defaultTo(true);
      table.timestamps(true, true);
    }),

    knex.schema.createTable("salon_address", function(table) {
      table.increments("id").primary();
      table
        .integer("salon_id")
        .unsigned()
        .references("salon_authentication.id");
      table.string("full_name");
      table.string("line1");
      table.string("line2");
      table.string("city");
      table.string("state");
      table.string("zip_code");
      table.string("country");
      table
        .boolean("verified")
        .notNullable()
        .defaultTo(false);
      table
        .boolean("reserved")
        .notNullable()
        .defaultTo(false);
      table.timestamps(true, true);
    }),

    knex.schema.createTable("salon_phone", function(table) {
      table.increments("id").primary();
      table
        .integer("salon_id")
        .unsigned()
        .references("salon_authentication.id");
      table
        .string("number")
        .unique()
        .notNullable();
      table.string("country_code");
      table.integer("last4");
      table.enu("type", ["home", "mobile", "work"]);
      table
        .boolean("verified")
        .notNullable()
        .defaultTo(false);
      table
        .boolean("reserved")
        .notNullable()
        .defaultTo(false);
      table.timestamps(true, true);
    }),

    knex.schema.createTable("salon_kyc_details", function(table) {
      table.increments("id").primary();
      table
        .integer("salon_id")
        .unsigned()
        .references("salon_authentication.id");
      table.string("document_type");
      table.string("document_unique_id");
      table.string("description");
      table.integer("document_id");
      table
        .boolean("verified")
        .notNullable()
        .defaultTo(false);
      table.timestamps(true, true);
    }),

    knex.schema.createTable("salon_payment_details", function(table) {
      table.increments("id").primary();
      table
        .integer("salon_id")
        .unsigned()
        .references("salon_authentication.id");
      table.string("payment_type");
      table.string("payment_unique_id");
      table.string("description");
      table.integer("document_id");
      table.string("fund_account_id");
      table
        .boolean("verified")
        .notNullable()
        .defaultTo(false);
      table.timestamps(true, true);
    }),
    knex.schema.createTable("referrals", function(table) {
      table.increments("id").primary();
      table.string("salon_id");
      table.string("referral_code");
      table.unique(["salon_id", "referral_code"]);
      table.string("free_task_count");
      table.boolean("max_used").defaultTo("false");
      table.timestamps(true, true);
    }),

    knex.schema.createTable("userReferrals", function(table) {
      table.increments("id").primary();
      table.string("referral_id");
      table.string("referred_to").unique();
      table.boolean("referee_subscribed").defaultTo("false");
      table.string("referee_task_count");
      table.timestamps(true, true);
    }),

    knex.schema.createTable("credits", function(table) {
      table.increments("id").primary();
      table.string("name");
      table.integer("maxCredit");
      table.string("description");
      table.integer("cost");
      table.integer("expirationPolicy");
      table.timestamps(true, true);
    }),

    knex.schema.createTable("userCredits", function(table) {
      table.increments("id").primary();
      table.integer("salon_id").references("salon_authentication.id");
      table.integer("credit_id").references("credits.id");
      table.date("activation_date");
      table.date("expiration_date");
      table.integer("initial_credits");
      table.integer("remaining_credits");
      table.integer("spent_credits");
      table.timestamps(true, true);
    }),

    knex.schema.createTable("userroles", function(table) {
      table.increments("id").primary();
      table
        .string("role")
        .unique()
        .notNullable();
      table.string("description");
      table.timestamps(true, true);
    }),

    knex.schema.createTable("emailcategory", function(table) {
      table.increments("id").primary();
      table
        .string("category")
        .unique()
        .notNullable();
      table.string("description");
      table.timestamps(true, true);
    }),

    knex.schema.createTable("salon_preferences", function(table) {
      table.increments("id").primary();
      table
        .integer("salon_id")
        .unsigned()
        .references("salon_authentication.id");
      table
        .integer("default_billing_address_id")
        .unsigned()
        .references("salon_address.id");
      table
        .integer("default_shipping_address_id")
        .unsigned()
        .references("salon_address.id");
      table
        .integer("primary_address_id")
        .unsigned()
        .references("salon_address.id");
      table
        .integer("primary_phone_id")
        .unsigned()
        .references("salon_phone.id");
      table
        .integer("primary_email_id")
        .unsigned()
        .references("salon_email.id");
      table
        .boolean("email_comm_ok")
        .notNullable()
        .defaultTo(true);
      table
        .boolean("phone_comm_ok")
        .notNullable()
        .defaultTo(true);

      table.timestamps(true, true);
    }),

    knex.schema.createTable("salon_profile", function(table) {
      table.increments("id").primary();
      table.integer("salon_id").references("salon_authentication.id");
      table.integer("primary_email_id").references("salon_email.id");
      table.integer("primary_phone_id").references("salon_phone.id");
      table.integer("primary_address_id").references("salon_address.id");
      table.string("display_image_url");
      table
        .boolean("is_individual")
        .notNullable()
        .defaultTo(false);
      table
        .boolean("is_details_filled")
        .notNullable()
        .defaultTo(false);
      table.enu("salon_type", ["M", "F", "BOTH"]);
      table.boolean("is_account_verified");
      table.boolean("is_kyc_verified");
      table.boolean("is_email_verified");
      table.boolean("is_referred").defaultTo(false);
      table.string("first_name");
      table.string("last_name");
      table.date("date_of_birth");
      table.string("user_name");
      table.string("organization_name");
      table.string("contact_id");
      table.bigInteger("startTime");
      table.bigInteger("endTime");
      table.bigInteger("breakStartTime");
      table.bigInteger("breakEndTime");
      table.string("weekly_holiday");
      table.boolean("new_user").defaultTo(true);
      table
        .boolean("is_active")
        .notNullable()
        .defaultTo(true);
      table.timestamps(true, true);
    }),

    knex.schema.createTable("emailpreferences", function(table) {
      table.increments("id").primary();
      table
        .integer("email_id")
        .unsigned()
        .references("salon_email.id");
      table
        .integer("email_category_id")
        .unsigned()
        .references("emailcategory.id");
      table
        .boolean("comm_ok")
        .notNullable()
        .defaultTo(true);
      table.unique(["email_id", "email_category_id"]);
      table.timestamps(true, true);
    }),

    knex.schema.createTable("services", function(table) {
      table.increments("id").primary();
      table.string("service_name");
      table.string("service_display_image_url");
      table
        .boolean("is_combo")
        .notNullable()
        .defaultTo(false);
      table.integer("duration");
      table.string("category");
      table.timestamps(true, true);
    }),

    knex.schema.createTable("professional", function(table) {
      table.increments("id").primary();
      table.string("first_name");
      table.string("last_name");
      table.string("display_image_url");
      table
        .boolean("is_bank_verified")
        .notNullable()
        .defaultTo(false);
      table
        .boolean("is_kyc_verified")
        .notNullable()
        .defaultTo(false);
      table.integer("salon_id").references("salon_authentication.id");
      table.string("phone_number");
      table.string("list_of_service_ids");
      table.timestamps(true, true);
    }),

    knex.schema.createTable("bookedslots", function(table) {
      table.increments("id").primary();
      table
        .integer("salon_id")
        .unsigned()
        .references("salon_authentication.id");
      table
        .integer("professional_id")
        .unsigned()
        .references("professional.id");
      table.date("date_of_booking");
      table.bigInteger("startTime");
      table.bigInteger("endTime");
      table
        .boolean("is_booked")
        .notNullable()
        .defaultTo(false);
      table
        .boolean("is_unavailable")
        .notNullable()
        .defaultTo(false);
      table
        .boolean("is_salon_holiday")
        .notNullable()
        .defaultTo(false);
      table.timestamps(true, true);
    }),

    knex.schema.createTable("salon_services_map", function(table) {
      table.increments("id").primary();
      table.integer("rate");
      table.integer("athome_additional_rate");
      table
        .boolean("is_athome_offered")
        .notNullable()
        .defaultTo(false);
      table.string("currency");
      table.integer("salon_id").references("salon_authentication.id");
      table.integer("service_id").references("services.id");
      table.integer("duration");
      table.timestamps(true, true);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists("salon_preferences"),
    knex.schema.dropTableIfExists("emailpreferences"),
    knex.schema.dropTableIfExists("emailcategory"),
    knex.schema.dropTableIfExists("userroles"),
    knex.schema.dropTableIfExists("salon_payment_details"),
    knex.schema.dropTableIfExists("salon_kyc_details"),
    knex.schema.dropTableIfExists("salon_phone"),
    knex.schema.dropTableIfExists("salon_address"),
    knex.schema.dropTableIfExists("salon_email"),
    knex.schema.dropTableIfExists("salon_profile"),
    knex.schema.dropTableIfExists("salon_authentication"),
    knex.schema.dropTableIfExists("services"),
    knex.schema.dropTableIfExists("salon_services_map"),
    knex.schema.dropTableIfExists("professional"),
    knex.schema.dropTableIfExists("bookedslots")
  ]);
};
